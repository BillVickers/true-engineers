Read me:

True Engineers 

This application will allow Customers to register, log in, log out, and 
display my resume.

ASP.NET 4.5.2, ASP.NET MVC 5, Entity Framework 6 and Domain Driven Design

System Requirements:

Visual Studio Enterprise 2015 with Update 1
.NET Framework 4.0, 4.5, 4.5.2 and 4.6


This ASP.NET MVC application uses features like:

1) Code First Migrations
2) Entity Framework and LINQ
3) Razor view engine
4) Custom Membership Provider pointing to your own database users table.
5) Partial views and partial actions
6) Html Helpers
7) Data Annotation validation
8) Attribute Routing
9) Querying View Models


Altering connectionStrings section:

Based on convention, EF will look for a connection string named as the DBContext 
in this case "DefaultConnection"), and will use it, so feel free to set the data provider you want.
If you are running the project locally you need to use the update-database command in the Package Manger Console 
to have Entity Framework create a local database on your machine for this project. 

<!-- 
     By default (convention over configuration, the connection string with the same name as your DBContext will be used 
     You can select then wherever you will use SQL CE, SQL Serer Express Edition, etc, here. 
 -->
<connectionStrings>
    <add name="DefaultConnection" connectionString="Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-NewTrueEngineers-20151214014012.mdf;Initial Catalog=aspnet-NewTrueEngineers-20151214014012;Integrated Security=True"
      providerName="System.Data.SqlClient" />
  </connectionStrings>
  
  
The Authors:

Bill Vickers

TrueEngineers.net

bill-vickers@att.net
12 Cobia St, Ponte Vedra Beach, FL 32082
904-412-9526
LinkedIn:  https://www.linkedin.com/in/william-vickers-624b1078

EDUCATION
University of North Florida, Jacksonville, FL
•	BS in Computer and Information Science	Expected Graduation: Spring 2016
•	BA in Education 				1997



  
  