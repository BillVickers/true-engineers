﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewTrueEngineers.Models
{
    public class Idea
    {
        [Key]
        public int IdeaId { get; set; }
        [Required(ErrorMessage = "Your Post is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Devotional")]
        public string IdeaPost { get; set; }
        public int Rating { get; set; }
        [Display(Name = "Post Date")]
        public DateTime PostDate { get; set; }

        [ForeignKey("CreatedByUser")]
        public virtual string CreatedById { get; set; }
        public ApplicationUser CreatedByUser { get; set; }
    }
}
