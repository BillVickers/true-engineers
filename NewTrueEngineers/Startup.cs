﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewTrueEngineers.Startup))]
namespace NewTrueEngineers
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
